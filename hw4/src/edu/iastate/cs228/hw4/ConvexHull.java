package edu.iastate.cs228.hw4;

/**
 * @author Benjamin Fenton
 */

import java.util.ArrayList;
//import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/**
 * This class implements Graham's scan that constructs the convex hull of a
 * finite set of points.
 */

public class ConvexHull {

	// ---------------
	// Data Structures 
	// ---------------

	/**
	 * The array points[] holds an input set of Points, which may be randomly
	 * generated or input from a file. Duplicates may appear.
	 */
	private Point[] points;
	private int numPoints; // size of points[]

	/**
	 * Lowest point from points[]; and in case of a tie, the leftmost one of all
	 * such points. To be set by the private method lowestPoint().
	 */
	private Point lowestPoint;

	/**
	 * This array stores the same set of points from points[] with all
	 * duplicates removed.
	 */
	private Point[] pointsNoDuplicate;
	private int numDistinctPoints; // size of pointsNoDuplicate[]

	/**
	 * Points on which Graham's scan is performed. They are copied from
	 * pointsNoDuplicate[] with some points removed. More specifically, if
	 * multiple points from the array pointsNoDuplicate[] have the same polar
	 * angle with respect to lowestPoint, only the one furthest away from
	 * lowestPoint is included.
	 */
	private Point[] pointsToScan;
	private int numPointsToScan; // size of pointsToScan[]

	/**
	 * Vertices of the convex hull in counterclockwise order are stored in the
	 * array hullVertices[], with hullVertices[0] storing lowestPoint.
	 */
	private Point[] hullVertices;
	private int numHullVertices; // number of vertices on the convex hull

	/**
	 * Stack used by Grahma's scan to store the vertices of the convex hull of
	 * the points scanned so far. At the end of the scan, it stores the hull
	 * vertices in the counterclockwise order.
	 */
	private PureStack<Point> vertexStack;

	// ------------
	// Constructors
	// ------------

	/**
	 * Generate n random points within the box range [-50, 50] x [-50, 50].
	 * Duplicates are allowed. Store the points in the private array points[].
	 * 
	 * @param n
	 *            >= 1; otherwise, exception thrown.
	 */
	public ConvexHull(int n) throws IllegalArgumentException {
		// check n >= 1
		if(n < 1) {
			throw new IllegalArgumentException("Cannot have less than 1 point.");
		}
		// set numPoints after checking n is a valid number
		numPoints = n;

		// initialize points with corrent length
		points = new Point[numPoints];

		// generate random Points and fill points array
		for(int i = 0; i < numPoints; i++) {
			Random r = new Random();
			Point temp = new Point(r.nextInt(101) - 50, r.nextInt(101) - 50);
			points[i] = temp;
		}
	}

	/**
	 * Read integers from an input file. Every pair of integers represent the x-
	 * and y-coordinates of a point. Generate the points and store them in the
	 * private array points[]. The total number of integers in the file must be
	 * even.
	 * 
	 * You may declare a Scanner object and call its methods such as hasNext(),
	 * hasNextInt() and nextInt(). An ArrayList may be used to store the input
	 * integers as they are read in from the file.
	 * 
	 * @param inputFileName
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 *             when the input file contains an odd number of integers
	 */
	public ConvexHull(String inputFileName) throws FileNotFoundException, InputMismatchException {

		File inFile = new File(inputFileName);
		Scanner parser = new Scanner(inFile);

		ArrayList<Integer> intList = new ArrayList<Integer>();

		// add ints to ArrayList
		while(parser.hasNextInt()) {
			intList.add(parser.nextInt());
		}

		parser.close();

		// check if intList has odd number of ints
		if(intList.size() % 2 != 0) { // odd number if ints
			throw new InputMismatchException("Cannot have odd number of integers in file.");
		}

		// set numPoints
		numPoints = intList.size() / 2;

		// initialize points[] with correct size
		points = new Point[numPoints];

		// index of ints
		int j = 0;

		// populate points[]
		for(int i = 0; i < numPoints; i++) {
			int x = intList.get(j);
			j++;
			int y = intList.get(j);
			j++;

			Point temp = new Point(x, y);
			points[i] = temp;
		}
	}

	// -------------
	// Graham's scan
	// -------------

	/**
	 * This method carries out Graham's scan in several steps below:
	 * 
	 * 1) Call the private method lowestPoint() to find the lowest point from
	 * the input point set and store it in the variable lowestPoint.
	 * 
	 * 2) Call the private method setUpScan() to sort all points by polar angle
	 * with respect to lowestPoint. After elimination of all duplicates in
	 * points[], the points are stored in pointsNoDuplicate[]. Next, for
	 * multiple points having the same polar angle with respect to lowestPoint,
	 * keep only the one furthest from lowestPoint. The points after the second
	 * round of elimination are stored in the array pointsToScan[].
	 * 
	 * 3) Perform Graham's scan on the points in the array pointsToScan[]. To
	 * initialize the scan, push pointsToScan[0] and pointsToScan[1] onto
	 * vertexStack.
	 * 
	 * 4) As the scan terminates, vertexStack holds the vertices of the convex
	 * hull. Pop the vertices out of the stack and add them to the array
	 * hullVertices[], starting at index numHullVertices - 1, and decreasing the
	 * index toward 0. Set numHullVertices.
	 * 
	 * Two special cases below must be handled:
	 * 
	 * 1) The array pointsToScan[] could contain just one point, in which case
	 * the convex hull is the point itself.
	 * 
	 * 2) Or it could contain two points, in which case the hull is the line
	 * segment connecting them.
	 */
	public void GrahamScan() {
		// find lowest point
		lowestPoint();

		// set up scan
		setUpScan();

		//create stack
		vertexStack = new ArrayBasedStack<Point>();

		// handle special cases of 1 point or 2 points
		if(pointsToScan.length == 1) {
			numHullVertices = 1;
			hullVertices = new Point[numHullVertices];
			hullVertices[0] = pointsToScan[0];
			return;
		} else if(pointsToScan.length == 2) {
			numHullVertices = 2;
			hullVertices = new Point[numHullVertices];
			hullVertices[0] = pointsToScan[0];
			hullVertices[1] = pointsToScan[1];
			return;
		}

		// run scan normally
		//initialize stack
		vertexStack.push(pointsToScan[0]);
		vertexStack.push(pointsToScan[1]);
		vertexStack.push(pointsToScan[2]);

		// check if points are a right turn, and add/remove from stack
		for(int i = 3; i < pointsToScan.length; i++) {

			Point next = pointsToScan[i];
			Point top = vertexStack.pop();
			Point nextToTop = vertexStack.peek();

			PointComparator comp = new PointComparator(nextToTop);

			while(comp.comparePolarAngle(top, next) == 1) { // cross product is negative
				top = vertexStack.pop();
				nextToTop = vertexStack.peek();
				comp = new PointComparator(nextToTop);
			}
			
			vertexStack.push(top);
			vertexStack.push(next);
		}

		// update numHullVertices, add points to hull vertices array
		numHullVertices = vertexStack.size();
		hullVertices = new Point[numHullVertices];

		for(int i = hullVertices.length - 1; i >= 0; i--) {
			hullVertices[i] = vertexStack.pop();
		}
	}

	// ------------------------------------------------------------
	// toString() and Files for Convex Hull Plotting in Mathematica
	// ------------------------------------------------------------

	/**
	 * The string displays the convex hull with vertices in counter clockwise
	 * order starting at lowestPoint. When printed out, it will list five points
	 * per line with three blanks in between. Every point appears in the format
	 * "(x, y)".
	 * 
	 * For illustration, the convex hull example in the project description will
	 * have its toString() generate the output below:
	 * 
	 * (-7, -10) (0, -10) (10, 5) (0, 8) (-10, 0)
	 * 
	 * lowestPoint is listed only ONCE.
	 */
	public String toString() {

		String list = "";

		for(int i = 0; i < hullVertices.length; i++) {

			list += hullVertices[i];

			if(i + 1 != hullVertices.length) {
				if((i + 1) % 5 == 0) {
					list += "\n";
				} else {
					list += "   ";
				}
			}
		}

		return list;
	}

	/**
	 * For plotting in Mathematica.
	 * 
	 * Writes to the file "hull.txt" the vertices of the constructed convex hull
	 * in counterclockwise order for rendering in Mathematica. These vertices
	 * are in the array hullVertices[], starting at lowestPoint. Every line in
	 * the file displays the x and y coordinates of only one point. Write the
	 * coordinates of lowestPoint again to end the file.
	 * 
	 * For instance, the file "hull.txt" generated for the convex hull example
	 * in the project description will have the following content:
	 * 
	 * -7 -10 0 -10 10 5 0 8 -10 0 -7 -10
	 * 
	 * Note that lowestPoint (-7, -10) has its coordinates listed in the first
	 * and last lines. This is for Mathematica to plot the hull as a polygon
	 * rather than one missing the edge connecting (-10, 0) and (-7, -10).
	 * 
	 * Called only after GrahamScan().
	 * 
	 * 
	 * @throws IllegalStateException
	 *             if hullVertices[] has not been populated (i.e., the convex
	 *             hull has not been constructed)
	 */
	public void hullToFile() throws IllegalStateException {
		
		if(hullVertices == null) {
			throw new IllegalStateException("No hull to write.");
		}
		
		File outFile = new File("hull.txt");
		
		try {
			PrintWriter writer = new PrintWriter(outFile);
			
			// write vertices to file
			for(int i = 0; i < hullVertices.length; i++) {
				String point = "";
				point += hullVertices[i].getX();
				point += " ";
				point += hullVertices[i].getY();
				writer.println(point);
			}
			
			// write start point to complete hull
			String point = "";
			point += hullVertices[0].getX();
			point += " ";
			point += hullVertices[0].getY();
			writer.println(point);
			
			// close writer
			writer.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist.");
		}
	}

	/**
	 * For plotting in Mathematica.
	 * 
	 * Writes to the file "points.txt" the points stored in the array
	 * pointsNoDuplicate[]. The format is the same as required for the method
	 * hullToFile(), except that the coordinates of lowestPoint appear only
	 * once.
	 * 
	 * Called only after setUpScan() or GrahamScan().
	 * 
	 * @throws IllegalStateException
	 *             if pointsNoDuplicate[] has not been populated.
	 */
	public void pointsToFile() throws IllegalStateException {
		
		if(pointsNoDuplicate == null) {
			throw new IllegalStateException("No points to write.");
		}
		
		File outFile = new File("points.txt");
		
		try {
			PrintWriter writer = new PrintWriter(outFile);
			
			// write vertices to file
			for(int i = 0; i < pointsNoDuplicate.length; i++) {
				String point = "";
				point += pointsNoDuplicate[i].getX();
				point += " ";
				point += pointsNoDuplicate[i].getY();
				writer.println(point);
			}
			
			// close writer
			writer.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist.");
		}
	}

	/**
	 * Also implement this method for testing purpose.
	 * 
	 * Writes to the file "pointsScanned.txt" the points stored in the array
	 * pointsToScan[]. The format is the same as required for the method
	 * pointsToFile().
	 * 
	 * Called only after setUpScan() or GrahamScan().
	 * 
	 * @throws IllegalStateException
	 *             if pointsToScan[] has not been populated.
	 */
	public void pointsScannedToFile() throws IllegalStateException {
		
		if(pointsToScan == null) {
			throw new IllegalStateException("No points to write.");
		}
		
		File outFile = new File("pointsScanned.txt");
		
		try {
			PrintWriter writer = new PrintWriter(outFile);
			
			// write vertices to file
			for(int i = 0; i < pointsToScan.length; i++) {
				String point = "";
				point += pointsToScan[i].getX();
				point += " ";
				point += pointsToScan[i].getY();
				writer.println(point);
			}
			
			// close writer
			writer.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist.");
		}
	}

	// ---------------
	// Private Methods
	// ---------------

	/**
	 * Find the point in the array points[] that has the smallest y-coordinate.
	 * In case of a tie, pick the point with the smallest x-coordinate. Set the
	 * variable lowestPoint to the found point.
	 * 
	 * Multiple elements from points[] could coincide at the lowestPoint (i.e.,
	 * they are the same point). This situation could happen, though with a very
	 * small chance. In this case, any of them can be lowestPoint.
	 * 
	 * Ought to be private, but is made public for testing convenience.
	 */
	public void lowestPoint() {
		// index of lowest point in points[]
		int lowestIdx = 0;

		for(int i = 0; i < points.length; i++) {
			if(points[i].getY() <= points[lowestIdx].getY()) {
				lowestIdx = i;
			} else if(points[i].getY() == points[lowestIdx].getY()) {
				if(points[i].getX() <= points[lowestIdx].getX()) {
					lowestIdx = i;
				}
			}
		}

		// set lowestPoint
		lowestPoint = points[lowestIdx];
	}

	/**
	 * Call quickSort() on points[]. After the sorting, duplicates in points[]
	 * will appear next to each other, with those equal to lowestPoint at the
	 * beginning of the array.
	 * 
	 * Copy the points from points[] into the array pointsNoDuplicate[],
	 * eliminating all duplicates. Update numDistinctPoints.
	 * 
	 * Copy the points from pointsNoDuplicate[] into the array pointsToScan[]
	 * and eliminate some as follows. If multiple points have the same polar
	 * angle, eliminate all but the one that is the furthest from lowestPoint.
	 * Update numPointsToScan.
	 * 
	 * Ought to be private, but is made public for testing convenience.
	 *
	 */
	public void setUpScan() {

		// sort points
		quickSort();

		// eliminate duplicates
		ArrayList<Point> noDups = new ArrayList<Point>();
		Point previousP = points[0];
		noDups.add(previousP);

		for(int i = 1; i < points.length; i++) {
			Point check = points[i];

			if(!previousP.equals(check)) { // add if not equal
				noDups.add(check);
			}

			previousP = check;
		}

		// update numDistinctPoints
		numDistinctPoints = noDups.size();

		// convert to Point array
		pointsNoDuplicate = new Point[numDistinctPoints];
		noDups.toArray(pointsNoDuplicate);

		// eliminate points with same angle
		ArrayList<Point> diffAngles = new ArrayList<Point>();
		
		// add first point
		Point previousA = pointsNoDuplicate[0];
		diffAngles.add(previousA);
		
		// make comparator based on lowestPoint
		PointComparator comp = new PointComparator(lowestPoint);

		// add points, if same polar angle remove closer point
		for(int i = 1; i < pointsNoDuplicate.length; i++) {

			Point check = pointsNoDuplicate[i];

			if(comp.comparePolarAngle(previousA, check) == 0) {
				diffAngles.remove(diffAngles.size() - 1);
			}
			
			diffAngles.add(check);
			previousA = check;
		}

		// update numPointstoScan
		numPointsToScan = diffAngles.size();

		// add to pointsToScan
		pointsToScan = new Point[numPointsToScan];
		diffAngles.toArray(pointsToScan);

	}

	/**
	 * Sort the array points[] in the increasing order of polar angle with
	 * respect to lowestPoint. Use quickSort. Construct an object of the
	 * pointComparator class with lowestPoint as the argument for point
	 * comparison.
	 * 
	 * Ought to be private, but is made public for testing convenience.
	 */
	public void quickSort() {
		quickSortRec(0, points.length - 1);
	}

	/**
	 * Operates on the subarray of points[] with indices between first and last.
	 * 
	 * @param first
	 *            starting index of the subarray
	 * @param last
	 *            ending index of the subarray
	 */
	private void quickSortRec(int first, int last) {
		if(first < last) {
			int p = partition(first, last);
			quickSortRec(first, p - 1);
			quickSortRec(p + 1, last);
		}
	}

	/**
	 * Operates on the subarray of points[] with indices between first and last.
	 * 
	 * @param first
	 * @param last
	 * @return
	 */
	private int partition(int first, int last) {

		PointComparator comp = new PointComparator(lowestPoint);

		// random pivot between first and last
		Random r = new Random();
		int pivIndex = r.nextInt((last - first) + 1) + first;
		Point pivot = points[pivIndex];

		// swap pivot to end
		Point temp = points[pivIndex];
		points[pivIndex] = points[last];
		points[last] = temp;

		int i = first - 1;

		for(int j = first; j < last; j++) {
			if(comp.compare(points[j], pivot) == -1) {
				i++;
				// swap points[i] and points[j]
				temp = points[i];
				points[i] = points[j];
				points[j] = temp;
			}
		}

		// swap points[i + 1] and points[last]
		temp = points[i + 1];
		points[i + 1] = points[last];
		points[last] = temp;

		return i + 1;
	}

}
