package edu.iastate.cs228.hw4;

/**
 * @author Benjamin Fenton
 */

import java.util.Comparator;

/**
 * This class compares two points p1 and p2 by polar angle with respect to a
 * reference point. It is known that the reference point is not above either p1
 * or p2, and in the case that either or both of p1 and p2 have the same
 * y-coordinate, not to their right.
 */
public class PointComparator implements Comparator<Point> {

	private Point referencePoint;

	/**
	 * 
	 * @param p
	 *            reference point
	 */
	public PointComparator(Point p) {
		referencePoint = p;
	}

	/**
	 * Use cross product and dot product to implement this method. Do not take
	 * square roots or use trigonometric functions. See the PowerPoint notes on
	 * how to carry out cross and dot products.
	 * 
	 * Call comparePolarAngle() and compareDistance().
	 * 
	 * @param p1
	 * @param p2
	 * @return -1 if one of the following three conditions holds:
	 * 
	 *         a) p1 and referencePoint are the same point but p2 is a different
	 *         point
	 * 
	 *         b) neither p1 nor p2 equals referencePoint, and the polar angle
	 *         of p1 with respect to referencePoint is less than that of p2
	 * 
	 *         c) neither p1 nor p2 equals referencePoint, p1 and p2 have the
	 *         same polar angle w.r.t. referencePoint, and p1 is closer to
	 *         referencePoint than p2.
	 * 
	 *         0 if p1 and p2 are the same point
	 * 
	 *         1 if one of the following three conditions holds:
	 * 
	 *         a) p2 and referencePoint are the same point but p1 is a different
	 *         point
	 * 
	 *         b) neither p1 nor p2 equals referencePoint, and the polar angle
	 *         of p1 with respect to referencePoint is greater than that of p2
	 * 
	 *         c) neither p1 nor p2 equals referencePoint, p1 and p2 have the
	 *         same polar angle w.r.t. referencePoint, and p1 is further from
	 *         referencePoint than p2.
	 * 
	 */
	public int compare(Point p1, Point p2) {
		
		// compare points by angle and distance
		int compAngle = comparePolarAngle(p1, p2);
		int compDistance = compareDistance(p1, p2);
		
		// recenter vectors based on referencePoint
		p1 = recenter(referencePoint, p1);
		p2 = recenter(referencePoint, p2);
		
		// check if p1 is same point as p2
		boolean samePoint = false;
		
		if(p1.getX() == p2.getX() && p1.getY() == p2.getX()) {
			samePoint = true;
		}
		
		// check if points are same as reference point
		// check if p1 is reference point (should be 0,0 after recentering)
		boolean p1Check = false;

		if(p1.getX() == 0 && p1.getY() == 0) { // p1 is same as referencePoint
			p1Check = true;
		}

		// check if p2 is reference point (would be 0,0 after recentering)
		boolean p2Check = false;

		if(p2.getX() == 0 && p2.getY() == 0) { // p2 is same as referencePoint
			p2Check = true;
		}
		
		if((p1Check && !samePoint) || ((!p1Check && !p2Check) && (compAngle == -1 ||(compAngle == 0 && compDistance == -1)))) {
			return -1;
		} else if((p2Check && !samePoint) || ((!p1Check && !p2Check) && (compAngle == 1 ||(compAngle == 0 && compDistance == 1)))) {
			return 1;
		} else { // same point (same angle and distance)
			return 0;
		}
		
	}

	/**
	 * Compare the polar angles of two points p1 and p2 with respect to
	 * referencePoint. Use cross products. Do not use trigonometric functions.
	 * 
	 * Precondition: p1 and p2 are distinct points.
	 * 
	 * @param p1
	 * @param p2
	 * @return -1 if p1 equals referencePoint or its polar angle with respect to
	 *         referencePoint is less than that of p2.
	 * 
	 *         0 if p1 and p2 have the same polar angle.
	 * 
	 *         1 if p2 equals referencePoint or its polar angle with respect to
	 *         referencePoint is less than that of p1.
	 */
	public int comparePolarAngle(Point p1, Point p2) {
		
		// recenter vectors based on referencePoint
		p1 = recenter(referencePoint, p1);
		p2 = recenter(referencePoint, p2);

		int angle = (p1.getX() * p2.getY()) - (p2.getX() * p1.getY());

		// check if p1 is reference point (should be 0,0 after recentering)
		boolean p1Check = false;

		if(p1.getX() == 0 && p1.getY() == 0) { // p1 is same as referencePoint
			p1Check = true;
		}

		// check if p2 is reference point (would be 0,0 after recentering)
		boolean p2Check = false;

		if(p2.getX() == 0 && p2.getY() == 0) { // p2 is same as referencePoint
			p2Check = true;
		}

		// return results of comparisons
		if(p1Check || angle > 0) { // p1 = refPoint or p1 cross p2 is positive (angle p1 < angle p2)
			return -1;
		} else if(p2Check || angle < 0) { // p2 = refPoint or p1 cross p2 is negative (angle p1 > angle p2)
			return 1;
		} else {
			return 0; // same polar angle and not same as reference point
		}
	}

	/**
	 * Compare the distances of two points p1 and p2 to referencePoint. Use dot
	 * products. Do not take square roots.
	 * 
	 * @param p1
	 * @param p2
	 * @return -1 if p1 is closer to referencePoint
	 * 
	 *         0 if p1 and p2 are equidistant to referencePoint
	 * 
	 *         1 if p2 is closer to referencePoint
	 */
	public int compareDistance(Point p1, Point p2) {
		
		// recenter vectors based on referencePoint
		p1 = recenter(referencePoint, p1);
		p2 = recenter(referencePoint, p2);
		
		// get square of distance, if dist1 > dist2, dist1^2 > dist2^2 (eliminate need to find square root)
		int dist1 = (p1.getX() * p1.getX()) + (p1.getY() * p1.getY());
		int dist2 = (p2.getX() * p2.getX()) + (p2.getY() * p2.getY());

		if(dist1 < dist2) { // p1 is closer to reference point
			return -1;
		} else if(dist1 > dist2) { // p2 is closer to reference point
			return 1;
		} else { // p1 and p2 are equidistant from reference point
			return 0;
		}
	}
	
	private Point recenter(Point referencePoint, Point p1) {
		// recenter vectors based on referencePoint
		Point recentered = new Point(p1.getX() - referencePoint.getX(), p1.getY() - referencePoint.getY());
		
		return recentered;
	}
}
