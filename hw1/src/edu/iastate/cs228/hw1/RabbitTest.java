package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @Author Benjamin Fenton
 */

public class RabbitTest {
	
	private World w1;
	private World w12;

	private Living r;
	
	@Test
	public void checkWho() {

		r = new Rabbit(null, 0, 0, 0);

		assertEquals("Should be a rabbit.", r.who(), State.RABBIT);
	}
	
	@Test
	public void checkOldAge() throws FileNotFoundException {

		w1 = new World("R1.txt");
		w12 = new World(w1.getWidth());

		w1.grid[1][1] = new Badger(w1, 1, 1, Living.RABBIT_MAX_AGE);

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty, rabbit died.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkRabbitStarves() throws FileNotFoundException {

		w1 = new World("R2.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkRabbitForDinner() throws FileNotFoundException {

		w1 = new World("R3.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkRabbitSurvives() throws FileNotFoundException {

		w1 = new World("R1.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a rabbit.", State.RABBIT, w12.grid[1][1].who());
	}
}
