package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class PredatorPreyTest {
	
	private World w1;
	private World w12;
	private World w2;
	private World w22;
	
	@Before
	public void setup() throws FileNotFoundException {
		w1 = new World("PPT1.txt");
		w12 = new World(w1.getWidth());
		w2 = new World("PPT2.txt");
		w22 = new World(w2.getWidth());
	}
	
	@Test
	public void checkUpdateWorld1() {
		
		String ans = "B E G\nB E E\nR E E";
		
		PredatorPrey.updateWorld(w1, w12);
		
		assertEquals("World must have correct result.", w12.toString(), ans);
	}
	
	@Test
	public void checkUpdateWorld2() {
		String ans = "G G R R E R\nG F R R E R\nG G R R R E\nG E R E E R\nG G R R E R\nG G G R B E";
		
		PredatorPrey.updateWorld(w2, w22);
		PredatorPrey.updateWorld(w22, w2);
		PredatorPrey.updateWorld(w2, w22);
		PredatorPrey.updateWorld(w22, w2);
		PredatorPrey.updateWorld(w2, w22);
		PredatorPrey.updateWorld(w22, w2);
		PredatorPrey.updateWorld(w2, w22);
		PredatorPrey.updateWorld(w22, w2);
		
		assertEquals("World must have correct result.", w2.toString(), ans);
	}

}
