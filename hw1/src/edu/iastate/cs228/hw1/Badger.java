package edu.iastate.cs228.hw1;

/**
 * @Author Benjamin Fenton
 */

/**
 * A badger eats rabbits and competes against a fox.
 */
public class Badger extends Living {

	private int age;

	/**
	 * Constructor
	 * 
	 * @param w
	 *            world
	 * @param r
	 *            row position
	 * @param c
	 *            column position
	 * @param a
	 *            age
	 */
	public Badger(World w, int r, int c, int a) {

		this.world = w;
		this.row = r;
		this.column = c;
		age = a;

	}

	/**
	 * A badger occupies the square.
	 */
	public State who() {
		return State.BADGER;
	}

	/**
	 * A badger dies of old age or hunger, or from an attack by a group of foxes
	 * when alone.
	 * 
	 * @param wNew
	 *            world of the next cycle
	 * @return Living life form occupying the square in the next cycle.
	 */
	public Living next(World wNew) {

		// See Living.java for an outline of the function.
		// See the project description for the survival rules for a fox.

		int[] pop = new int[NUM_LIFE_FORMS];

		census(pop);

		if(age == BADGER_MAX_AGE) {
			Living nextLiving = new Empty(wNew, row, column);
			return nextLiving;
		} else if(pop[BADGER] == 1 && pop[FOX] > 1) {
			Living nextLiving = new Fox(wNew, row, column, 0);
			return nextLiving;
		} else if(pop[BADGER] + pop[FOX] > pop[RABBIT]) {
			Living nextLiving = new Empty(wNew, row, column);
			return nextLiving;
		} else {
			Living nextLiving = new Badger(wNew, row, column, age + 1);
			return nextLiving;
		}
	}

	@Override
	public String toString() {
		return "B";
	}
}
