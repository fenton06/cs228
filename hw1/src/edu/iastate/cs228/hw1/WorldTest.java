package edu.iastate.cs228.hw1;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class WorldTest {
	
	private World w1;
	
	@Before
	public void setup(){
		w1 = new World(3);
	}
	
	@Test
	public void checkWorldWidth() {
		assertEquals("Width should be 3.", 3, w1.getWidth());
	}
	
	@Test
	public void checkWorldFilled() {
		
		int filled = 0;
		
		for(int i = 0; i < w1.getWidth(); i++) {
			for(int j = 0; j < w1.getWidth(); j++) {
				if(w1.grid[i][j] != null) {
					filled++;
				}
			}
		}
		
		assertEquals("World should be array of nulls.", 0, filled);
		
	}
	
	@Test
	public void checkRandFill() {
		
		w1.randomInit();
		
		boolean filled = true;
		
		for(int i = 0; i < w1.getWidth(); i++) {
			for(int j = 0; j < w1.getWidth(); j++) {
				if(w1.grid[i][j] == null) {
					filled = false;
					break;
				}
			}
			if(filled == false) {
				break;
			}
		}
		
		assertEquals("Random initialization should have no null values.", true, filled);
	}
	
	@Test
	public void checkRandInit() {

		World w2 = new World(w1.getWidth());
		
		w1.randomInit();
		assertEquals("Randomly generated worlds should be different!", false, (w1.toString()).equals(w2.toString()));
	}
	
	@Test
	public void checkWriteFileCreation() throws FileNotFoundException {
		
		w1.randomInit();
		
		w1.write("output.txt");
		
		boolean created = false;
		
		File f = new File("output.txt");
		if(f.exists() && !f.isDirectory()) {
			created = true;
		}
		
		assertEquals("File should be created.", true, created);
	}
	
	@Test
	public void checkWriteContents() throws FileNotFoundException{
		
		w1 = new World("PPT1.txt");
		
		w1.write("output.txt");
		
		World w2 = new World("output.txt");
		
		assertEquals("Output should match World used to create it.", true, (w1.toString()).equals(w2.toString()));
	}
}
