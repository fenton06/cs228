package edu.iastate.cs228.hw1;



import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @Author Benjamin Fenton
 */

/**
 * The PredatorPrey class does the predator-prey simulation over a grid world
 * with squares occupied by badgers, foxes, rabbits, grass, or none.
 */
public class PredatorPrey {

	/**
	 * Update the new world from the old world in one cycle.
	 * 
	 * @param wOld
	 *            old world
	 * @param wNew
	 *            new world
	 */
	public static void updateWorld(World wOld, World wNew) {

		// For every life form (i.e., a Living object) in the grid wOld,
		// generate a Living object in the grid wNew at the corresponding
		// location such that the former life form changes into the latter life form.
		//
		// Employ the method next() of the Living class.

		for(int i = 0; i < wOld.getWidth(); i++) {
			for(int j = 0; j < wOld.getWidth(); j++) {
				wNew.grid[i][j] = wOld.grid[i][j].next(wNew);
			}
		}

	}

	/**
	 * Repeatedly generates worlds either randomly or from reading files. Over
	 * each world, carries out an input number of cycles of evolution.
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException {

		// Generate predator-prey simulations repeatedly like shown in the
		// sample run in the project description.
		//
		// 1. Enter 1 to generate a random world, 2 to read a world from an
		// input file, and 3 to end the simulation. (An input file always ends
		// with the suffix .txt.)
		//
		// 2. Print out standard messages as given in the project description.
		//
		// 3. For convenience, you may define two worlds even and odd as below.
		// In an even numbered cycle (starting at zero), generate the world
		// odd from the world even; in an odd numbered cycle, generate even
		// from odd.

		System.out.println("The Predator-Prey Simulator");
		System.out.println("Keys: 1 (Random World)   2 (File Input)   3 (exit)\n");

		int trial = 1;

		Scanner in = new Scanner(System.in);

		System.out.println("Trial " + trial + ": ");

		while(in.hasNextLine()) {

			int cycles;
			int choice = Integer.parseInt(in.nextLine());

			switch(choice) {
			case 1:
				System.out.println("Random World");

				System.out.println("Enter grid width: ");
				int width = Integer.parseInt(in.nextLine());

				System.out.println("Enter number of cycles: ");
				cycles = Integer.parseInt(in.nextLine());

				randomWorld(width, cycles);
				break;

			case 2:
				System.out.println("World Input from File");

				System.out.println("File name:");
				String fileName = in.nextLine();

				System.out.println("Enter number of cycles: ");
				cycles = Integer.parseInt(in.nextLine());

				fileWorld(fileName, cycles);
				break;
			default:
				return;

			}

			trial++;
			System.out.println("Trial " + trial + ": ");

		}

		in.close();

		// 4. Print out initial and final worlds only. No intermediate worlds
		// should appear in the standard output. (When debugging your program, you can
		// print intermediate worlds.)
		//
		// 5. You may save some randomly generated worlds as your own test cases.
		//
		// 6. It is not necessary to handle file input & output exceptions for
		// this project. Assume data in an input file to be correctly formated.

	}

	private static void randomWorld(int width, int cycles) throws FileNotFoundException {

		World even = new World(width); // the world after an even number of
										// cycles
		World odd = new World(even.getWidth()); // the world after an odd number
												// of cycles
		even.randomInit();

		runSim(even, odd, cycles);
	}

	private static void fileWorld(String fileName, int cycles) throws FileNotFoundException {

		World even = new World(fileName); // the world after an even number of cycles
		World odd = new World(even.getWidth()); // the world after an odd number of cycles

		runSim(even, odd, cycles);
	}

	private static void runSim(World even, World odd, int cycles) throws FileNotFoundException {

		System.out.println("Initial world:\n");
		System.out.println(even + "\n");

		for(int i = 0; i < cycles; i++) {
			if(i % 2 == 0) {
				updateWorld(even, odd);
			} else {
				updateWorld(odd, even);
			}
		}

		System.out.println("Final world:\n");
		if(cycles % 2 == 0) {
			System.out.println(even + "\n");
			even.write("output.txt");
		} else {
			System.out.println(odd + "\n");
			odd.write("output.txt");
		}

	}

}
