package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class BadgerTest {

	private World w1;
	private World w12;

	private Living b;

	@Test
	public void checkWho() {
		b = new Badger(null, 1, 1, 0);

		assertEquals("Should be a fox.", b.who(), State.BADGER);
	}

	@Test
	public void checkOldAge() throws FileNotFoundException {

		w1 = new World("B1.txt");
		w12 = new World(w1.getWidth());

		w1.grid[1][1] = new Badger(w1, 1, 1, Living.BADGER_MAX_AGE);

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty, badger died.", State.EMPTY, w12.grid[1][1].who());
	}

	@Test
	public void checkFoxTakeover() throws FileNotFoundException {

		w1 = new World("B2.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a fox.", State.FOX, w12.grid[1][1].who());
	}

	@Test
	public void checkRabbitsOutnumbered() throws FileNotFoundException {

		w1 = new World("B3.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}

	@Test
	public void checkBadgerSurvives() throws FileNotFoundException {

		w1 = new World("B1.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a badger.", w1.grid[1][1].who(), State.BADGER);
	}
}
