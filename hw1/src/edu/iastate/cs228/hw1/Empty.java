package edu.iastate.cs228.hw1;

/**
 * @Author Benjamin Fenton
 */

/**
 * Empty squares are competed by various forms of life.
 */
public class Empty extends Living {

	public Empty(World w, int r, int c) {
		this.world = w;
		this.row = r;
		this.column = c;
	}

	public State who() {
		return State.EMPTY;
	}

	/**
	 * An empty square will be occupied by Badger, Fox, Rabbit, or Grass, or
	 * remain empty.
	 * 
	 * @param wNew
	 *            world of the next life cycle.
	 * @return Living life form in the next cycle.
	 */
	public Living next(World wNew) {

		// See Living.java for an outline of the function.
		// See the project description for corresponding survival rules.

		int[] pop = new int[NUM_LIFE_FORMS];

		census(pop);

		if(pop[RABBIT] > 1) {
			Living nextLiving = new Rabbit(wNew, row, column, 0);
			return nextLiving;
		} else if(pop[FOX] > 1) {
			Living nextLiving = new Fox(wNew, row, column, 0);
			return nextLiving;
		} else if(pop[BADGER] > 1) {
			Living nextLiving = new Badger(wNew, row, column, 0);
			return nextLiving;
		} else if(pop[GRASS] >= 1) {
			Living nextLiving = new Grass(wNew, row, column);
			return nextLiving;
		} else {
			Living nextLiving = new Empty(wNew, row, column);
			return nextLiving;
		}

	}

	@Override
	public String toString() {
		return "E";
	}
}
