package edu.iastate.cs228.hw1;

/**
 * @Author Benjamin Fenton
 */

/**
 * Grass remains if more than rabbits in the neighborhood; otherwise, it is
 * eaten.
 *
 */
public class Grass extends Living {

	public Grass(World w, int r, int c) {
		this.world = w;
		this.row = r;
		this.column = c;
	}

	public State who() {
		return State.GRASS;
	}

	/**
	 * Grass can be eaten out by rabbits in the neighborhood.
	 */
	public Living next(World wNew) {

		// See Living.java for an outline of the function.
		// See the project description for the survival rules for grass.

		int[] pop = new int[NUM_LIFE_FORMS];

		census(pop);

		if(pop[RABBIT] >= pop[GRASS] * 2) {
			Living nextLiving = new Empty(wNew, row, column);
			return nextLiving;
		} else if(pop[RABBIT] > pop[GRASS]) {
			Living nextLiving = new Rabbit(wNew, row, column, 0);
			return nextLiving;
		} else {
			Living nextLiving = new Grass(wNew, row, column);
			return nextLiving;
		}

	}

	@Override
	public String toString() {
		return "G";
	}
}
