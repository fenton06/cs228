package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class LivingTest {
	
	private World w1;
	
	@Before
	public void setup() throws FileNotFoundException {
		w1 = new World("PPT1.txt");
	}
	
	@Test
	public void checkCensus1() {
		
		int[] pop = new int[Living.NUM_LIFE_FORMS];
		int[] ans = {2, 1, 2, 2, 2};
		
		w1.grid[1][1].census(pop);
		
		assertArrayEquals("Census must count all Livings in neighborhood.", ans, pop);
		
	}
	
	@Test
	public void checkCensus2() {
		
		int[] pop = new int[Living.NUM_LIFE_FORMS];
		int[] ans = {2, 1, 0, 0, 1};
		
		w1.grid[0][0].census(pop);
		
		assertArrayEquals("Census must not invalid neighbors", ans, pop);
	}
}
