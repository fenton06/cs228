package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class EmptyTest {

	private World w1;
	private World w12;

	private Living e;

	@Test
	public void checkWho() {

		e = new Empty(null, 0, 0);

		assertEquals("Should be empty.", e.who(), State.EMPTY);
	}
	
	@Test
	public void checkRabbitTakeover() throws FileNotFoundException {
		
		w1 = new World("E1.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a rabbit.", State.RABBIT, w12.grid[1][1].who());
	}
	
	@Test
	public void checkFoxTakeover() throws FileNotFoundException {
		
		w1 = new World("E2.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a fox.", State.FOX, w12.grid[1][1].who());
	}
	
	@Test
	public void checkBadgerTakeover() throws FileNotFoundException {
		
		w1 = new World("E3.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a badger.", State.BADGER, w12.grid[1][1].who());
	}
	
	@Test
	public void checkGrassTakeover() throws FileNotFoundException {
		
		w1 = new World("E4.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be grass.", State.GRASS, w12.grid[1][1].who());
	}
	
	@Test
	public void checkStayEmpty() throws FileNotFoundException {
		
		w1 = new World("E5.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}
}
