package edu.iastate.cs228.hw1;

/**
 * @Author Benjamin Fenton
 */

/**
 * Different forms of life.
 */
public enum State {
	BADGER, EMPTY, FOX, GRASS, RABBIT
}
