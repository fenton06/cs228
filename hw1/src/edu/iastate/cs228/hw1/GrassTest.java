package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @Author Benjamin Fenton
 */

public class GrassTest {
	
	private World w1;
	private World w12;

	private Living g;
	
	@Test
	public void checkWho() {

		g = new Grass(null, 0, 0);

		assertEquals("Should be grass.", g.who(), State.GRASS);
	}
	
	@Test
	public void checkEmpty() throws FileNotFoundException {
		
		w1 = new World("G1.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkRabbitTakeover() throws FileNotFoundException {
		
		w1 = new World("G2.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a Rabbit.", State.RABBIT, w12.grid[1][1].who());
	}
	
	@Test
	public void checkStayGrass() throws FileNotFoundException {
		
		w1 = new World("G3.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be grass.", State.GRASS, w12.grid[1][1].who());
	}

}
