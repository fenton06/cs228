package edu.iastate.cs228.hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Random;

/**
 * @Author Benjamin Fenton
 */

/**
 * The world is represented as a square grid of size width X width.
 */
public class World {

	private int width; // grid size: width X width

	public Living[][] grid;

	/**
	 * Default constructor reads from a file
	 */
	public World(String inputFileName) throws FileNotFoundException {

		// Assumption: The input file is in correct format.
		//
		// You may create the grid world in the following steps:
		//
		// 1) Reads the first line to determine the width of the grid.
		//
		// 2) Creates a grid object.
		//
		// 3) Fills in the grid according to the input file.
		//
		// Be sure to close the input file when you are done.

		File file = new File(inputFileName);
		Scanner parser = new Scanner(file);

		String line = parser.nextLine();
		String lineWidth = line.replaceAll(" ", "");
		width = lineWidth.length();
		grid = new Living[width][width];

		int i = 0;
		processWorldLine(line, i);
		i++;

		while(parser.hasNextLine()) {
			line = parser.nextLine();
			processWorldLine(line, i);
			++i;
		}

		parser.close();

	}

	/**
	 * Constructor that builds a w X w grid without initializing it.
	 * 
	 * @param width
	 *            the grid
	 */
	public World(int w) {

		width = w;
		grid = new Living[w][w];

	}

	public int getWidth() {
		return width;
	}

	/**
	 * Initialize the world by randomly assigning to every square of the grid
	 * one of BADGER, FOX, RABBIT, GRASS, or EMPTY.
	 */
	public void randomInit() {

		Random generator = new Random();

		for(int row = 0; row < width; row++) {
			for(int col = 0; col < width; col++) {

				int type = generator.nextInt(5);

				switch(type) {
				case 0:
					grid[row][col] = new Badger(this, row, col, 0);
					break;
				case 1:
					grid[row][col] = new Empty(this, row, col);
					break;
				case 2:
					grid[row][col] = new Fox(this, row, col, 0);
					break;
				case 3:
					grid[row][col] = new Grass(this, row, col);
					break;
				case 4:
					grid[row][col] = new Rabbit(this, row, col, 0);
					break;
				}
			}
		}

	}

	/**
	 * Write the world grid as a string according to the output format.
	 */
	@Override
	public String toString() {

		String s = "";

		for(int row = 0; row < width; row++) {
			for(int col = 0; col < width; col++) {
				s += grid[row][col];
				if(col + 1 < width) {
					s += ' ';
				} else if(row + 1 < width){
					s += '\n';
				}
			}
		}

		return s;
	}

	/**
	 * Write the world grid to an output file. Useful for a randomly generated
	 * world.
	 * 
	 * @throws FileNotFoundException
	 */
	public void write(String outputFileName) throws FileNotFoundException {

		// 1. Open the file.
		//
		// 2. Write to the file. The five life forms are represented by
		// characters
		// B, E, F, G, R. Leave one blank space in between. Examples are given
		// in the project description.
		//
		// 3. Close the file.

		File outFile = new File(outputFileName);
		PrintWriter out = new PrintWriter(outFile);

		out.println(this);

		out.close();

	}

	private void processWorldLine(String line, int i) {

		Scanner temp = new Scanner(line);

		for(int j = 0; j < width; j++) {
			String type = temp.next();

			switch(type) {
			case "B":
				grid[i][j] = new Badger(this, i, j, 0);
				break;
			case "E":
				grid[i][j] = new Empty(this, i, j);
				break;
			case "F":
				grid[i][j] = new Fox(this, i, j, 0);
				break;
			case "G":
				grid[i][j] = new Grass(this, i, j);
				break;
			case "R":
				grid[i][j] = new Rabbit(this, i, j, 0);
				break;
			}
		}
		temp.close();

	}
}
