package edu.iastate.cs228.hw1;

import java.io.FileNotFoundException;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author Benjamin Fenton
 */

public class FoxTest {

	private World w1;
	private World w12;

	private Living f;

	@Test
	public void checkWho() {

		f = new Fox(null, 0, 0, 0);

		assertEquals("Should be a fox.", f.who(), State.FOX);
	}
	
	@Test
	public void checkOldAge() throws FileNotFoundException {

		w1 = new World("F1.txt");
		w12 = new World(w1.getWidth());

		w1.grid[1][1] = new Fox(w1, 1, 1, Living.FOX_MAX_AGE);

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty, fox died.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkBadgerTakeover() throws FileNotFoundException {

		w1 = new World("F2.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a badger.", State.BADGER, w12.grid[1][1].who());
	}
	
	@Test
	public void checkRabbitOutnumbered() throws FileNotFoundException {

		w1 = new World("F3.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be empty.", State.EMPTY, w12.grid[1][1].who());
	}
	
	@Test
	public void checkFoxSurvivers() throws FileNotFoundException {

		w1 = new World("F4.txt");
		w12 = new World(w1.getWidth());

		PredatorPrey.updateWorld(w1, w12);

		assertEquals("Should be a fox.", State.FOX, w12.grid[1][1].who());
	}
}
