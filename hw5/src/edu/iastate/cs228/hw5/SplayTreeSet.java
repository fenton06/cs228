package edu.iastate.cs228.hw5;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Splay tree implementation of the Set interface. The contains() and remove()
 * methods of AbstractSet are overridden to search the tree without using the
 * iterator.
 */

public class SplayTreeSet<E extends Comparable<? super E>> extends AbstractSet<E> {

	// The root of the tree containing this set's items	
	Node<E> root;

	// The total number of elements stored in this set
	int size;

	/**
	 * Default constructor. Creates a new, empty, SplayTreeSet
	 */

	public SplayTreeSet() {

		root = null;
		size = 0;
	}

	/**
	 * Shallow copy constructor. This method is fully implemented and should not
	 * be modified.
	 */

	public SplayTreeSet(Node<E> root, int size) {
		this.root = root;
		this.size = size;
	}

	/**
	 * Gets the root of this tree. Used only for testing. This method is fully
	 * implemented and should not be modified.
	 * 
	 * @return the root of this tree.
	 */

	public Node<E> getRoot() {
		return root;
	}

	/**
	 * Determines whether the set contains an element. Splays at the node that
	 * stores the element. If the element is not found, splays at the last node
	 * on the search path.
	 * 
	 * @param obj
	 *            element to be determined whether to exist in the tree
	 * @return true if the element is contained in the tree and false otherwise
	 */

	@Override
	public boolean contains(Object obj) {
		
		// class & null checks
		if(obj == null || !obj.getClass().equals((root.getData()).getClass())) {
			return false;
		}

		if(root == null) { // empty tree
			return false;
		}

		@SuppressWarnings("unchecked")
		E key = (E) obj;

		Node<E> cont = findEntry(key);

		if(cont != null) {
			this.splay(cont);
			return true;
		} else {
			return false; // splay handled by findEntry if false;
		}
	}

	/**
	 * Inserts an element into the splay tree. In case the element was not
	 * contained, this creates a new node and splays the tree at the new node.
	 * If the element exists in the tree already, it splays at the node
	 * containing the element.
	 * 
	 * @param key
	 *            element to be inserted
	 * @return true if insertion is successful and false otherwise
	 */

	@Override
	public boolean add(E key) {

		// cannot add null value
		if(key == null) {
			throw new NullPointerException("Cannot add null key.");
		}

		// tree is empty (root == null)
		if(root == null) {
			root = new Node<E>(key);
			size++;
			return true;
		}

		Node<E> current = root;

		while(true) {
			// compare key to current node
			int comp = (current.getData()).compareTo(key);

			if(comp == 0) { // tree already contains key
				this.splay(current); // splay at node containing key
				return false;
			} else if(comp > 0) { // key is less than current
				if(current.getLeft() != null) {
					current = current.getLeft();
				} else {
					Node<E> toAdd = new Node<E>(key); // create node to add
					toAdd.setParent(current); // set parent of node to add
					current.setLeft(toAdd); // set new node as left of parent
					size++; // increase size

					current = current.getLeft(); // go to inserted node
					this.splay(current); // splay at inserted node
					return true;
				}
			} else { // key is greater than current
				if(current.getRight() != null) {
					current = current.getRight();
				} else {
					Node<E> toAdd = new Node<E>(key); // create node toAdd
					toAdd.setParent(current); // set parent of node toAdd
					current.setRight(toAdd); // set new node as right of parent
					size++; // increase size

					current = current.getRight(); // go to inserted node
					this.splay(current); // splay at inserted node
					return true;
				}
			}
		}
	}

	/**
	 * Removes the node that stores an element. Splays at its parent node after
	 * removal (No splay if the removed node was the root.) If the node was not
	 * found, the last node encountered on the search path is splayed to the
	 * root.
	 * 
	 * @param obj
	 *            element to be removed from the tree
	 * @return true if the object is removed and false if it was not contained
	 *         in the tree.
	 */

	@Override
	public boolean remove(Object obj) {

		@SuppressWarnings("unchecked")
		E key = (E) obj;

		Node<E> toRem = findEntry(key); // if not found, splayed at last encountered node

		if(toRem == null) { // key not found
			return false;
		}

		unlinkNode(toRem); // remove node

		Node<E> parent = toRem.getParent();

		if(parent != null) { // toRem is not root, splay at parent
			this.splay(parent);
		}

		return true;
	}

	/**
	 * Returns the node containing key, or null if the key is not found in the
	 * tree. Called by contains().
	 * 
	 * @param key
	 * @return the node containing key, or null if not found
	 */

	protected Node<E> findEntry(E key) {

		Node<E> current = root;
		Node<E> previous = null;

		while(current != null) {
			int comp = current.getData().compareTo(key);

			if(comp == 0) { // key is contained in tree
				return current;
			} else if(comp > 0) { // key is less than current
				previous = current;
				current = current.getLeft();
			} else { // key is greater than current
				previous = current;
				current = current.getRight();
			}
		}

		if(previous != null) { // root wasn't null (unempty list)
			this.splay(previous); // splay at node before current node (current is now null)
		}
		
		return null;
	}

	/**
	 * Returns the successor of the given node.
	 * 
	 * @param n
	 * @return the successor of the given node in this tree, or null if there is
	 *         no successor
	 */

	protected Node<E> successor(Node<E> n) {

		if(n == null) {
			return null;
		} else if(n.getRight() != null) {

			Node<E> current = n.getRight();

			while(current.getLeft() != null) { // get leftmost entry in right subtree
				current = current.getLeft();
			}

			return current;
		} else { // closest leftChild ancestor

			Node<E> current = n.getParent();
			Node<E> child = n;

			while(current != null && current.getRight() == child) {
				child = current;
				current = current.getParent();
			}

			return current; // current is either null, or child is leftchild of current			
		}
	}

	/**
	 * Removes the given node, preserving the binary search tree property of the
	 * tree.
	 * 
	 * @param n
	 *            node to be removed
	 */

	protected void unlinkNode(Node<E> n) {
		
		Node<E> parent = n.getParent(); // null if n == root
		
		// n has 2 children
		if(n.getLeft() != null && n.getRight() != null) {
			
			Node<E> leftChild = n.getLeft();
			Node<E> rightChild = n.getRight();
			
			// break leftChild link to n
			leftChild.setParent(null);
			
			// splay at largest node in left subtree
			Node<E> largestLeft = getLargestNode(leftChild);
			this.splay(largestLeft);
			
			// set right subtree of n as right child of largest node in left subtree
			rightChild.setParent(largestLeft);
			largestLeft.setRight(rightChild);
			
			// set parent of n's left child as largest left
			largestLeft.setParent(parent);
			
			if(parent != null) { // n was not root
				parent.setLeft(largestLeft);
				this.splay(parent); // splay at parent of removed node
			} else {
				root = largestLeft;
			}
			
		} else { // n has at most one child
			
			Node<E> replacement = null;

			if(n.getLeft() != null) {
				replacement = n.getLeft();
			} else if(n.getRight() != null) {
				replacement = n.getRight();
			}

			if(parent == null) { // n is root with only one child
				root = replacement;
			} else {
				if(n.isLeftChild()) { // parent is not root and n is left child
					parent.setLeft(replacement);
				} else { // parent is not root and n is right child
					parent.setRight(replacement);
				}
			}

			if(replacement != null) {
				replacement.setParent(parent);
			}
		}

		size--;
	}

	@Override
	public Iterator<E> iterator() {
		return new SplayTreeIterator();
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * Returns a representation of this tree as a multi-line string as explained
	 * in the project description.
	 */

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		toStringRec(root, sb, 0);

		String out = sb.toString();

		return out;
	}

	private void toStringRec(Node<E> n, StringBuilder sb, int depth) {

		for(int i = 0; i < depth; i++) {
			sb.append("    ");
		}

		if(n == null) {
			sb.append("null\n");
			return;
		}

		sb.append((n.getData()).toString());
		sb.append("\n");

		if(n.getLeft() != null || n.getRight() != null) {
			toStringRec(n.getLeft(), sb, depth + 1);
			toStringRec(n.getRight(), sb, depth + 1);
		}
	}

	/**
	 * Splay at the current node. This consists of a sequence of zig, zigZig, or
	 * zigZag operations until the current node is moved to the root of the
	 * tree.
	 * 
	 * @param current
	 *            node at which to splay.
	 */

	protected void splay(Node<E> current) {

		if(current.getParent() == null) { // current == root of tree or subtree
			return;
		}

		while(current.getParent() != null) { // not root of tree or subtree
			if((current.getParent()).getParent() == null) { // no grandparent
				this.zig(current);
			} else { // has grandparent
				// current and parent are both right or left children
				if((current.isLeftChild() && (current.getParent()).isLeftChild())
						|| (current.isRightChild() && (current.getParent()).isRightChild())) {
					this.zigZig(current);
				} else { // current and parent are opposite children
					this.zigZag(current);
				}
			}
		}
	}

	/**
	 * Performs the zig operation on a node.
	 * 
	 * @param current
	 *            node at which to perform the zig operation.
	 */

	protected void zig(Node<E> current) {

		Node<E> parent = current.getParent();

		if(current.isLeftChild()) { // rotate right
			
			Node<E> t2 = current.getRight();

			parent.setLeft(t2);
			
			if(t2 != null) {
				t2.setParent(parent);
			}

			current.setRight(parent);
			parent.setParent(current);

		} else { // rotate left
			
			Node<E> t2 = current.getLeft();

			parent.setRight(t2);
			if(t2 != null) {
				t2.setParent(parent);
			}

			current.setLeft(parent);
			parent.setParent(current);
		}
		
		
		if(parent == root) {
			root = current;
		}
		
		current.setParent(null);
	}

	/**
	 * Performs the zig-zig operation on a node.
	 * 
	 * @param current
	 *            node at which to perform the zig-zig operation.
	 */

	protected void zigZig(Node<E> current) {
		
		Node<E> parent = current.getParent();
		Node<E> grandparent = parent.getParent();
		Node<E> ggrand = grandparent.getParent(); // may or may not be null
		
		if(current.isLeftChild()) { // rotate right
			
			Node<E> t2 = current.getRight();
			Node<E> t3 = parent.getRight();
			
			// change t3 links
			grandparent.setLeft(t3);
			
			if(t3 != null) {
				t3.setParent(grandparent);
			}
			
			// set grandparent's parent to parent
			grandparent.setParent(parent);
			parent.setRight(grandparent);
			
			// change t2 links
			parent.setLeft(t2);
			
			if(t2 != null) {
				t2.setParent(parent);
			}
			
			// set parent's parent to current
			parent.setParent(current);
			current.setRight(parent);
			
		} else { // rotate left
			
			Node<E> t2 = current.getLeft();
			Node<E> t3 = parent.getLeft();
			
			// change t3 links
			grandparent.setRight(t3);
			
			if(t3 != null) {
				t3.setParent(grandparent);
			}
			
			// set grandparent's parent tp parent
			grandparent.setParent(parent);
			parent.setLeft(grandparent);
			
			// chage t2 links
			parent.setRight(t2);
			
			if(t2 != null) {
				t2.setParent(parent);
			}
			
			// set parent's parent to current
			parent.setParent(current);
			current.setLeft(parent);
		}
		
		if(grandparent == root) { // grandparent was root
			root = current;
			current.setParent(null);
		} else { // grandparent was not root
			if(ggrand.getLeft() == grandparent) {
				ggrand.setLeft(current);
			} else {
				ggrand.setRight(current);
			}
			current.setParent(ggrand);
		}
	}

	/**
	 * Performs the zig-zag operation on a node.
	 * 
	 * @param current
	 *            node to perform the zig-zag operation on
	 */

	protected void zigZag(Node<E> current) {
		
		Node<E> parent = current.getParent();
		Node<E> grandparent = parent.getParent();
		Node<E> ggrand = grandparent.getParent(); // may or may not be null
		
		Node<E> t2 = current.getLeft();
		Node<E> t3 = current.getRight();
		
		if(parent.isLeftChild() && current.isRightChild()) {
			
			// set left and right of current as parent and grandparent, respectively
			current.setLeft(parent);
			parent.setParent(current);
			
			current.setRight(grandparent);
			grandparent.setParent(current);
			
			// set t2 as right of parent
			parent.setRight(t2);
			
			if(t2 != null) {
				t2.setParent(parent);
			}
			
			// set t3 as left of grandparent
			grandparent.setLeft(t3);
			
			if(t3 != null) {
				t3.setParent(grandparent);
			}
		} else {
			
			// set left and right of current as grandparent and parent, respectively
			current.setLeft(grandparent);
			grandparent.setParent(current);
			
			current.setRight(parent);
			parent.setParent(current);
			
			//set t2 as right of grandparent
			grandparent.setRight(t2);
			
			if(t2 != null) {
				t2.setParent(grandparent);
			}
			
			// set t3 as left of parent
			parent.setLeft(t3);
			
			if(t3 != null) {
				t3.setParent(parent);
			}
			
			
		}

		if(grandparent == root) { // grandparent was root
			root = current;
			current.setParent(null);
		} else { // grandparent was not root
			if(ggrand.getLeft() == grandparent) {
				ggrand.setLeft(current);
			} else {
				ggrand.setRight(current);
			}
			current.setParent(ggrand);
		}
	}
	
	private Node<E> getSmallestNode(Node<E> root) {
		
		Node<E> current = root;
		
		if(current != null) {
			while(current.getLeft() != null) {
				current = current.getLeft();
			}
		}
		
		return current;
	}
	
	private Node<E> getLargestNode(Node<E> root) {
		
		Node<E> current = root;
		
		if(current != null) {
			while(current.getRight() != null) {
				current = current.getRight();
			}
		}
		
		return current;
		
	}

	/**
	 *
	 * Iterator implementation for this splay tree. The elements are returned in
	 * ascending order according to their natural ordering.
	 *
	 */

	private class SplayTreeIterator implements Iterator<E> {

		Node<E> cursor;

		private Node<E> pending; // stores last call to next(), null if no node available for remove()

		public SplayTreeIterator() {

			cursor = getSmallestNode(root);
		}

		@Override
		public boolean hasNext() {
			return cursor != null;
		}

		@Override
		public E next() {

			if(!hasNext()) {
				throw new NoSuchElementException();
			}

			pending = cursor;
			cursor = successor(cursor);

			return pending.getData();
		}

		@Override
		public void remove() {

			if(pending == null) { // no next was called before calling remove
				throw new IllegalStateException();
			}

			if(pending.getLeft() != null && pending.getRight() != null) {
				cursor = pending;
			}

			unlinkNode(pending);
			
			if(pending.getParent() != null) { // pending is not root
				splay(pending.getParent());
			}

			pending = null;
		}
	}
}