package edu.iastate.cs228.hw5;

import java.util.Iterator;
import java.util.ArrayList;

/**
 * An implementation of a map based on a splay tree.
 */
public class SplayTreeMap<K extends Comparable<? super K>, V> {

	/**
	 * The key-value pairs in this Map.
	 */
	private SplayTreeSet<MapEntry<K, V>> entrySet = new SplayTreeSet<MapEntry<K, V>>();

	/**
	 * Default constructor. Creates a new, empty, SplayTreeMap
	 */
	public SplayTreeMap() {
		
		entrySet = new SplayTreeSet<MapEntry<K, V>>();
		
	}

	/**
	 * Simple copy constructor used only for testing. This method is fully
	 * implemented and should not be modified.
	 */
	public SplayTreeMap(SplayTreeSet<MapEntry<K, V>> set) {
		entrySet = set;
	}

	/**
	 *
	 * @return the number of key-value mappings in this map.
	 */

	public int size() {
		
		int size = entrySet.size();
		
		return size;
	}

	/**
	 * @return the value to which key is mapped, or null if this map contains no
	 *         mapping for key.
	 */

	public V get(K key) {
		
		if(key == null) {
			return null;
		}
		
		Node<MapEntry<K, V>> n = entrySet.findEntry(new MapEntry<K, V>(key, null));
		
		if(n != null) {
			return n.getData().value;
		}
		
		return null;
	}

	/**
	 * @return true if this map contains a mapping for key.
	 */

	public boolean containsKey(K key) {
		
		if(key == null) {
			return false;
		}
		
		Node<MapEntry<K, V>> n = entrySet.findEntry(new MapEntry<K, V>(key, null));

		return n != null;
	}

	/**
	 * Associates value with key in this map.
	 *
	 * @return the previous value associated with key, or null if there was no
	 *         mapping for key.
	 */

	public V put(K key, V value) {
		
		if(key == null || value == null) {
			throw new NullPointerException("Cannot have null keys or values.");
		}
		
		// find node with matching key
		Node<MapEntry<K,V>> n = entrySet.findEntry(new MapEntry<K, V>(key, null));
		
		V ret = null;
		
		if(n != null) { // key is present; overwrite
			ret = (n.getData()).value; // save previous data to return
			n.getData().value = value;
		} else {
			entrySet.add(new MapEntry<K, V>(key, value));
		}

		return ret;
	}

	/**
	 * Removes the mapping for key from this map if it is present.
	 *
	 * @return the previous value associated with key, or null if there was no
	 *         mapping for key.
	 */

	public V remove(K key) {
		
		if(key == null) {
			return null;
		}
		
		// find node with matching key
		Node<MapEntry<K,V>> n = entrySet.findEntry(new MapEntry<K, V>(key, null));
		
		V ret = null;
		
		if(n != null) {
			ret = (n.getData()).value;
			entrySet.unlinkNode(n);
		}
		
		return ret;
	}

	/**
	 * @return a SplayTreeSet storing the keys contained in this map.
	 */

	public SplayTreeSet<K> keySet() {
		
		Iterator<MapEntry<K, V>> iter = entrySet.iterator();
		
		SplayTreeSet<K> keySet = new SplayTreeSet<K>();
		
		while(iter.hasNext()) {
			keySet.add((iter.next().key));
		}

		return keySet;
	}

	/**
	 * @return an ArrayList storing the values contained in this map. The values
	 *         in the ArrayList should be arranged in ascending order of the
	 *         corresponding keys.
	 */

	public ArrayList<V> values() {
		
		ArrayList<V> valuesArr = new ArrayList<V>();
		
		Iterator<MapEntry<K, V>> iter = entrySet.iterator();
		
		while(iter.hasNext()) {
			valuesArr.add((iter.next()).value);
		}

		return valuesArr;
	}
}