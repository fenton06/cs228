package edu.iastate.cs228.hw3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;
import java.util.Comparator;

/**
 * @author Benjamin Fenton
 */

/**
 * IMPORTANT: In the case of any minor discrepancy between the comments before a
 * method and its description in the file proj3.pdf, use the version from the
 * file.
 */

public class DoublySortedList {

	private int size; // number of different kinds of fruits
	private Node headN; // first node of the sorted linked list by fruit name
	private Node headB; // first node of the sorted linked list by bin number

	/**
	 * default constructor
	 */
	public DoublySortedList() {
		size = 0;
		headN = null;
		headB = null;
	}

	/**
	 * Constructor over an inventory file consists of lines in the following
	 * format
	 * 
	 * <fruit> <quantity> <bin>
	 * 
	 * Throws an exception if the file is not found.
	 * 
	 * You are asked to carry out the following operations:
	 * 
	 * 1. Scan line by line to construct one Node object for each fruit. 2.
	 * Create the two doubly-linked lists, by name and by bin number,
	 * respectively, on the fly as the scan proceeds. 3. Perform insertion sort
	 * on the two lists. Use the provided BinComparator and NameComparator
	 * classes to generate comparator objects for the sort.
	 * 
	 * @inventoryFile name of the file
	 */
	public DoublySortedList(String inventoryFile) throws FileNotFoundException {
		
		size = 0;

		File inFile = new File(inventoryFile);
		Scanner parser = new Scanner(inFile);

		// create head nodes
		String fruit = parser.next();
		int quantity = parser.nextInt();
		int bin = parser.nextInt();
		Node current = new Node(fruit, quantity, bin, null, null, null, null);
		headN = current;
		headN.nextN = headN;
		headN.previousN = headN;
		headB = current;
		headB.nextB = headB;
		headB.previousB = headB;
		size++;

		// build rest of list (unsorted)
		while(parser.hasNext()) {

			fruit = parser.next();
			quantity = parser.nextInt();
			bin = parser.nextInt();

			current.nextN = new Node(fruit, quantity, bin, headN, current, headB, current);
			// list unsorted, make next bin follow scanned order
			current.nextB = current.nextN;

			current = current.nextN;
			size++;
		}

		parser.close();

		// link last node to first node

		headN.previousN = current;
		headB.previousB = current;

		// sort by name
		insertionSort(true, new NameComparator());

		//sort by bin
		insertionSort(false, new BinComparator());

	}

	public int size() {
		return size;
	}

	/**
	 * Called by split() and also used for testing. The doubly sorted list has
	 * already been created.
	 * 
	 * @param size
	 * @param headN
	 * @param headB
	 */
	public DoublySortedList(int size, Node headN, Node headB) {
		this.size = size;
		this.headN = headN;
		this.headB = headB;
	}

	/**
	 * Add one type of fruits in given quantity (n).
	 * 
	 * 1. Search for the fruit. 2. If already stored in some node, simply
	 * increase the quantity by n 3. Otherwise, create a new node to store the
	 * fruit at the first available bin. add it to both linked lists by calling
	 * the helper methods insertN() and insertB(). 4. Modify the link headN
	 * and/or headB if the newly inserted node becomes the first of either DSL.
	 * (This is carried out by insertN() or insertB().)
	 * 
	 * The case n == 0 should result in no operation. The case n < 0 results in
	 * an exception thrown.
	 * 
	 * @param fruit
	 *            name of the fruit to be added
	 * @param n
	 *            quantity of the fruit
	 */
	public void add(String fruit, int n) throws IllegalArgumentException {
		// if adding negative quantity
		if(n < 0) {
			throw new IllegalArgumentException("Cannot add negative quantity.");
		} else if(n == 0) {
			return;
		}
		
		// if list is empty
		if(size == 0) {
			Node toInsert = newNode(fruit, n);
			headN = toInsert;
			headB = toInsert;
			headN.nextN = headN;
			headN.previousN = headN;
			headB.nextB = headB;
			headB.previousB = headB;
			size++;
			return;
		}

		Node current = headN;

		// find fruit if exists and add quantity
		do {
			if(current.fruit.equals(fruit)) {
				current.quantity += n;
				return;
			}
			current = current.nextN;
		} while(current != headN);
		
		Node toInsert = newNode(fruit, n);
		
		// add to name list
		current = headN;
		NameComparator compN = new NameComparator();

		do {
			int beforeCurrent = compN.compare(toInsert, current);

			if(beforeCurrent < 0) {
				break;
			}
			current = current.nextN;
		} while(current != headN);

		insertN(toInsert, current.previousN, current);

		// add to bin list
		current = headB;
		BinComparator compB = new BinComparator();
		do {
			int beforeCurrent = compB.compare(toInsert, current);

			if(beforeCurrent < 0) {
				break;
			}
			current = current.nextB;
		} while(current != headB);

		insertB(toInsert, current.previousB, current);

		
	}

	/**
	 * The fruit list is not sorted. For efficiency, you need to sort by name
	 * using quickSort. Maintain two arrays fruitName[] and fruitQuant[].
	 *
	 * After sorting, add the fruits to the doubly-sorted list (see project
	 * description) using the linear time algorithm described in Section 3.2 of
	 * the notes.
	 *
	 * Throws an IllegalArgumentException if the quantity specified for some
	 * fruit in fruitFile is negative. Ignore a fruit if its quantity in
	 * fruitFile is zero.
	 *
	 * @param fruitFile
	 *            list of fruits with quantities. one type of fruit followed by
	 *            its quantity per line.
	 */

	public void restock(String fruitFile) throws FileNotFoundException, IllegalArgumentException {

		File inFile = new File(fruitFile);
		Scanner scanLines = new Scanner(inFile);

		int numFruits = 0;

		// determine length of file for arrays
		while(scanLines.hasNextLine()) {
			numFruits++;
			scanLines.nextLine();
		}

		scanLines.close();

		// build arrays
		Scanner parser = new Scanner(inFile);

		String[] fruitName = new String[numFruits];
		Integer[] fruitQuant = new Integer[fruitName.length];

		for(int i = 0; i < fruitName.length; i++) {
			fruitName[i] = parser.next();
			fruitQuant[i] = parser.nextInt();
		}

		parser.close();
		
		// sort arrays
		quickSort(fruitName, fruitQuant, numFruits);
		
		// update quantities or add fruit
		Node currentN = headN;
		Node currentB = headB;
		
		for(int i = 0; i < fruitName.length; i++) {
			// find corresponding place in name list
			do {
				if(currentN == headN && currentN.fruit.compareTo(fruitName[i]) >= 0) {
					break;
				}else if(currentN.previousN.fruit.compareTo(fruitName[i]) < 0 && currentN.fruit.compareTo(fruitName[i]) >= 0) {
					break;
				}
				currentN = currentN.nextN;
			} while(currentN != headN);
			
			//update quantity if fruit is found
			if(currentN.fruit.equals(fruitName[i])) {
				currentN.quantity += fruitQuant[i];
				currentN = currentN.nextN;
			} else {
				// fruit doesn't exist, create node
				Node toInsert = newNode(fruitName[i], fruitQuant[i]);
				//insert into N list
				insertN(toInsert, currentN.previousN, currentN);
				
				// find where to insert into B list
				do {
					if(currentB.bin < toInsert.bin) {
						break;
					}
					currentB = currentB.nextB;
				} while(currentB != headB);
				
				// insert into B list
				insertB(toInsert, currentB.previousB, currentB);
			}
		}
	}

	/**
	 * Remove a fruit from the inventory.
	 * 
	 * 1. Search for the fruit on the N-list. 2. If no existence, make no
	 * change. 3. Otherwise, call the private method remove() on the node that
	 * stores the fruit to remove it.
	 * 
	 * @param fruit
	 */
	public void remove(String fruit) {
		
		if(size == 0) {
			return;
		}
		
		Node current = headN;

		do {
			if(current.fruit.equals(fruit)) {
				remove(current);
			}
			current = current.nextN;
		} while(current != headN);
	}

	/**
	 * Remove all fruits stored in the bin. Essentially, remove the node. The
	 * steps are as follows: 1. Search for the node with the bin in the B-list.
	 * 2. No change if it is not found. 3. Otherwise, call remove() on the found
	 * node.
	 * 
	 * @param bin
	 *            >= 1 (otherwise, throw an exception)
	 */
	public void remove(int bin) throws IllegalArgumentException {

		if(bin < 1) {
			throw new IllegalArgumentException("Bin does not exist.");
		}
		
		if(size == 0) {
			return;
		}

		Node current = headB;

		do {
			if(current.bin == bin) {
				remove(current);
			}
			current = current.nextB;
		} while(current != headB);

	}

	/**
	 * Sell n units of a fruit.
	 * 
	 * Search the N-list for the fruit. Return in the case no fruit is found.
	 * Otherwise, a Node node is located. Perform the following:
	 * 
	 * 1. if n >= node.quantity, call remove(node). 2. else, decrease
	 * node.quantity by n.
	 * 
	 * Throw an exception if n < 0.
	 * 
	 * @param fruit
	 * @param n
	 */
	public void sell(String fruit, int n) throws IllegalArgumentException {

		if(n < 0) {
			throw new IllegalArgumentException("Quantity less than zero.");
		}
		
		if(size == 0) {
			return;
		}

		Node current = headN;

		do {
			if(current.fruit.equals(fruit)) {
				if(n < current.quantity) {
					current.quantity -= n;
				} else {
					remove(fruit);
				}
			}
			current = current.nextN;
		} while(current != headN);
	}

	/**
	 * Process an order for multiple fruits as follows.
	 * 
	 * 1. Sort the ordered fruits and their quantities by fruit name using the
	 * private method quickSort(). 2. Traverse the N-list. Whenever a node with
	 * the next needed fruit is encountered, let m be the total number of a type
	 * of fruit to be ordered, and do the following: a) if m < 0, throw an
	 * exception; b) if m == 0, ignore. c) if 0 < m < node.quantity, decrease
	 * node.quantity by n. d) if m >= node.quanity, call remove(node).
	 * 
	 * @param fruitFile
	 */
	public void bulkSell(String fruitFile) throws FileNotFoundException, IllegalArgumentException {
		
		if(size == 0) {
			return;
		}

		File inFile = new File(fruitFile);
		Scanner scanLines = new Scanner(inFile);

		int numFruits = 0;

		// determine length of file for arrays
		while(scanLines.hasNextLine()) {
			numFruits++;
			scanLines.nextLine();
		}

		scanLines.close();

		// build arrays
		Scanner parser = new Scanner(inFile);

		String[] fruitName = new String[numFruits];
		Integer[] fruitQuant = new Integer[fruitName.length];

		for(int i = 0; i < fruitName.length; i++) {
			fruitName[i] = parser.next();
			fruitQuant[i] = parser.nextInt();
		}

		parser.close();
		
		// sort arrays
		quickSort(fruitName, fruitQuant, numFruits);
		
		// update quantities
		Node currentN = headN;
		
		for(int i = 0; i < fruitName.length; i++) {
			
			if(fruitQuant[i] < 0) {
				throw new IllegalArgumentException("Cannot sell negative quantity.");
			} else if (fruitQuant[i] != 0) {
				
				// find corresponding place in name list
				do {
					if(currentN == headN && currentN.fruit.compareTo(fruitName[i]) >= 0) {
						break;
					}else if(currentN.previousN.fruit.compareTo(fruitName[i]) < 0 && currentN.fruit.compareTo(fruitName[i]) >= 0) {
						break;
					}
					currentN = currentN.nextN;
				} while(currentN != headN);
				
				//update quantity if fruit is found (ignore if fruit isn't on stock list)
				if(currentN.fruit.equals(fruitName[i])) {
					// update if sell quant is < stock quant
					if(fruitQuant[i] < currentN.quantity) {
						currentN.quantity -= fruitQuant[i];
						currentN = currentN.nextN;
					// sell quant is >= to stock quant
					} else {
						remove(currentN);
						currentN = currentN.nextN;
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param fruit
	 * @return quantity of the fruit (zero if not on stock)
	 */
	public int inquire(String fruit) {
		
		int inStock = 0;
		
		if(size == 0) {
			return inStock;
		}

		Node current = headN;

		do {
			if(current.fruit.equals(fruit)) {
				inStock = current.quantity;
				break;
			}
			current = current.nextN;
		} while(current != headN);

		return inStock;
	}

	/**
	 * Output a string that gets printed out as the inventory of fruits by
	 * names. The exact format is given in Section 5.1. Here is a sample:
	 *
	 * 
	 * fruit quantity bin --------------------------- apple 50 5 banana 20 9
	 * grape 100 8 pear 40 3
	 */
	public String printInventoryN() {

		//Hard coded header
		String list = "fruit           quantity        bin\n-----------------------------------\n";
		
		if(headN == null) {
			return list;
		}

		Node current = headN;

		// add rest of list
		do {
			list += current.toString();
			if(current.nextN != headN) {
				list += "\n";
			}
			current = current.nextN;
		} while(current != headN);

		return list;
	}

	/**
	 * Output a string that gets printed out as the inventory of fruits by
	 * storage bin. Use the same formatting rules for printInventoryN(). Below
	 * is a sample:
	 * 
	 * bin fruit quantity ---------------------------- 3 pear 40 5 apple 50 8
	 * grape 100 9 banana 20
	 * 
	 */
	public String printInventoryB() {

		// hard code header
		String list = "bin             fruit           quantity\n----------------------------------------\n";
		
		if(headB == null) {
			return list;
		}

		Node current = headB;

		// add rest of list
		do {
			list += toStringB(current.fruit, current.quantity, current.bin);
			if(current.nextB != headB) {
				list += "\n";
			}
			current = current.nextB;
		} while(current != headB);

		return list;
	}

	@Override
	/**
	 *  The returned string should be printed out according to the format in Section 5.1, 
	 *  exactly the same required for printInventoryN(). 
	 */
	public String toString() {
		return printInventoryN();
	}

	/**
	 * Relocate fruits to consecutive bins starting at bin 1. Need only operate
	 * on the B-list.
	 */
	// 
	public void compactStorage() {
		
		if(size == 0) {
			return;
		}

		int count = 1;
		Node current = headB;

		do {
			current.bin = count;
			current = current.nextB;
			count++;
		} while(current != headB);

	}

	/**
	 * Remove all nodes by setting headN = null and headB = null.
	 */
	public void clearStorage() {
		this.headN = null;
		this.headB = null;
		size = 0;
	}

	/**
	 * Split the list into two doubly-sorted lists DST1 and DST2. Let N be the
	 * total number of fruit types. Then DST1 will contain the first N/2 types
	 * fruits in the alphabetical order. DST2 will contain the remaining fruit
	 * types. The algorithm works as follows.
	 * 
	 * 1. Traverse the N-list to find the median of the fruits by name. 2. Split
	 * at the median into two lists: DST1 and DST2. 3. Traverse the B-list. For
	 * every node encountered add it to the B-list of DST1 or DST2 by comparing
	 * node.fruit with the name of the median fruit.
	 * 
	 * @return the two doubly-sorted lists DST1 and DST2 as a pair.
	 */
	public Pair<DoublySortedList> split() { 

		DoublySortedList DST1 = new DoublySortedList();
		DoublySortedList DST2 = new DoublySortedList();
		
		if(size == 0) {
			return new Pair<DoublySortedList>(DST1, DST2);
		}
		
		DST1.headN = this.headN;
		DST1.size = this.size/2;
		DST2.size = this.size - (this.size/2);
		
		Node current = this.headN;
		
		int place = 1;
		
		// find end of first list
		do {
			current = current.nextN;
			place++;
		} while(place < DST1.size);
		
		//set previousN of DST1.headN to current (new tail)
		DST1.headN.previousN = current;
		
		// set median for comparing when making new B lists
		Node median = current;
		
		current = current.nextN;
		
		// set nextN of DST1 tail to DST1.headN 
		current.previousN.nextN = DST1.headN; 
		
		// set current to head of DST2
		DST2.headN = current;
		
		// find end of list
		while(current.nextN != headN){
			current = current.nextN;
		}
		
		// set DST2.headN.previousN to current
		DST2.headN.previousN = current;
		
		// set DST2.headN as nextN of DST2 tail
		current.nextN = DST2.headN;
		
		// create B lists for DST1 and DST2
		NameComparator compN = new NameComparator();
		
		current = this.headB;
		boolean first1 = true;
		boolean first2 = true;
		
		Node currentB1 = null;
		Node currentB2 = null;
		
		// cycle through B list
		do {
			// before or equal to median
			if(compN.compare(current, median) <= 0) {
				if(first1 == true) {
					DST1.headB = current;
					currentB1 = DST1.headB;
					first1 = false;
				} else {
					currentB1.nextB = current;
					currentB1 = currentB1.nextB;
				}
			
			// after median
			} else {
				if(first2 == true) {
					DST2.headB = current;
					currentB2 = DST2.headB;
					first2 = false;
				} else {
					currentB2.nextB = current;
					currentB2 = currentB2.nextB;
				}
				
				
				
			}
			current = current.nextB;
		} while(current != headB);
		
		// link last node to head node
		currentB1.nextB = DST1.headB;
		currentB2.nextB = DST2.headB;
		
		// link head node to last node
		DST1.headB.previousB = currentB1;
		DST2.headB.previousB = currentB2;
		
	 	return new Pair<DoublySortedList>(DST1, DST2); 
	}

	/**
	 * Perform insertion sort on the doubly linked list with head node using a
	 * comparator object, which is of either the NameComparator or the
	 * BinComparator class.
	 * 
	 * Made a public method for testing by TA.
	 * 
	 * @param name
	 *            sort the N-list if true and the B-list otherwise.
	 * @param comp
	 */
	public void insertionSort(boolean NList, Comparator<Node> comp) {
		
		// already sorted if size is 0 or 1
		if(size == 0 || size == 1) {
			return;
		}

		if(NList == true) {

			Node moving = headN.nextN;
			Node check = moving.previousN;
			Node current = moving.nextN;

			do {
				// remove moving node from N list
				moving.nextN.previousN = moving.previousN;
				moving.previousN.nextN = moving.nextN;
				size--;

				//find where node should be inserted
				while(check != headN && comp.compare(check, moving) > 0) {
					check = check.previousN;
				}

				insertN(moving, check, check.nextN);

				moving = current;
				check = moving.previousN;
				current = current.nextN;

			} while(moving != headN);

		} else {

			Node moving = headB.nextB;
			Node check = moving.previousB;
			Node current = moving.nextB;

			do {
				// remove moving node from B list
				moving.nextB.previousB = moving.previousB;
				moving.previousB.nextB = moving.nextB;

				//find where node should be inserted
				while(check != headB && comp.compare(check, moving) > 0) {
					check = check.previousB;
				}

				insertB(moving, check, check.nextB);

				moving = current;
				check = moving.previousB;
				current = current.nextB;

			} while(moving != headB);
		}
	}

	/**
	 * Sort an array of fruit names using quicksort. After sorting, bin[i] is
	 * the storage for fruit name[i].
	 * 
	 * Made a public method for testing by TA.
	 * 
	 * @param size
	 *            number of fruit names
	 * @param fruit
	 *            array of fruit names
	 * @param quant
	 *            array of fruit quantities
	 */
	public void quickSort(String fruit[], Integer quant[], int size) {
		
		if(size == 0 || size == 1) {
			return;
		}
		
		realQuickSort(fruit, quant, 0, size-1);
	}

	// --------------
	// helper methods 
	// --------------

	private void realQuickSort(String fruit[], Integer quant[], int low, int high) {
		
		if(low < high) {
			int p = partition(fruit, quant, low, high);
			realQuickSort(fruit, quant, low, p - 1);
			realQuickSort(fruit, quant, p + 1, high);
			
		}
		
	}
	
	/**
	 * Add a node between two nodes prev and next in the N-list. Update headN if
	 * the added node becomes the first node on the list in the alphabetical
	 * order of fruit name.
	 * 
	 * @param node
	 * @param prev
	 * @param next
	 */
	private void insertN(Node node, Node prev, Node next) {

		if(size == 0) {
			headN = node;
		} else {
			prev.nextN = node;
			node.previousN = prev;
			next.previousN = node;
			node.nextN = next;
		}

		// check if node should be headN
		NameComparator compN = new NameComparator();
		int comp = compN.compare(node, headN);

		if(comp < 0) {
			headN = node;
		}

		size++;
	}

	/**
	 * Add a node between two nodes prev and next in the B-list. Update headB if
	 * the added node becomes the first node on the list in the order of bin
	 * number.
	 * 
	 * @param node
	 * @param prev
	 * @param next
	 */
	private void insertB(Node node, Node prev, Node next) {

		if(size == 0) {
			headB = node;
		} else {
			prev.nextB = node;
			node.previousB = prev;
			next.previousB = node;
			node.nextB = next;
		}

		// check if node should be headB
		BinComparator compB = new BinComparator();
		int comp = compB.compare(node, headB);

		if(comp < 0) {
			headB = node;
		}
	}

	/**
	 * Remove node from both linked lists. Check if node is headN or headB, and
	 * reset the corresponding link if yes. If the both lists become empty, set
	 * headN and headB to null.
	 * 
	 * @param node
	 */
	private void remove(Node node) {

		// if size is 1 (node is both heads)
		if(size == 1) {
			headN = null;
			headB = null;
			return;
		}

		// break links pointing towards node
		node.nextN.previousN = node.previousN;
		node.previousN.nextN = node.nextN;
		node.nextB.previousB = node.previousB;
		node.previousB.nextB = node.nextB;

		// if node is headN
		if(node == headN) {
			headN = headN.nextN;
		}

		// if node is headB
		if(node == headB) {
			headB = headB.nextB;
		}
		
		size--;
	}

	/**
	 * 
	 * @param name
	 *            name[first, last] is the subarray of fruit names
	 * @param bin
	 *            bin[first, last] is the subarray of bins storing the fruits.
	 * @param first
	 * @param last
	 */
	private int partition(String fruit[], Integer quant[], int first, int last) {

		// random pivot between first and last
		Random r = new Random();
		int pivIndex = r.nextInt((last - first) + 1) + first;
		String pivot = fruit[pivIndex];
		
		//swap pivot to end (mirror everything in quant)
		String tempS = fruit[pivIndex];
		Integer tempI = quant[pivIndex];
		fruit[pivIndex] = fruit[last];
		quant[pivIndex] = quant[last];
		fruit[last] = tempS;
		quant[last] = tempI;
		
    	int i = first - 1;
    	
    	for(int j = first; j < last; j++) { 
    		if(fruit[j].compareTo(pivot) < 0) {
    			i++;
    			// swap fruit[i] and fruit[j] (mirror in quant)
    			tempS = fruit[i];
    			tempI = quant[i];
    			fruit[i] = fruit[j];
    			quant[i] = quant[j];
    			fruit[j] = tempS;
    			quant[j] = tempI;
    		}
    	}
    	
    	//swap fruit[i+1] and fruit[last] (mirror in quant)
    	tempS = fruit[i+1];
    	tempI = quant[i+1];
    	fruit[i+1] = fruit[last];
    	quant[i+1] = quant[last];
    	fruit[last] = tempS;
    	quant[last] = tempI;
    	
    	return i+1;
	}

	private String toStringB(String fruit, int quantity, int bin) {

		String info = "";
		info += bin;

		for(int i = 16 - String.valueOf(bin).length(); i > 0; i--) {
			info += " ";
		}

		info += fruit;

		for(int i = 16 - fruit.length(); i > 0; i--) {
			info += " ";
		}

		info += quantity;

		return info;
	}
	
	private Node newNode(String fruit, int quantity) {
		// find first available bin
		int firstBin = 1;
		
		// size = 0
		if(size == 0) {
			// create new node
			Node newNode = new Node(fruit, quantity, firstBin, null, null, null, null);
			
			return newNode;
		}
		
		Node current = headB;

		do {
			if(firstBin < current.bin) {
				break;
			}
			firstBin++;
			current = current.nextB;
		} while(current != headB);

		// create new node
		Node newNode = new Node(fruit, quantity, firstBin, null, null, null, null);
		
		return newNode;
	}
}
