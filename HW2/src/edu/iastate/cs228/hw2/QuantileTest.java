package edu.iastate.cs228.hw2;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test Quantiles.java.
 * 
 * @author Benjamin Fenton
 */
public class QuantileTest {

	private int[] arr = { 15, 17, 9, 35, 23, 2, 11, 18, 5, 6 };
	private Quantiles q4;
	private Quantiles q3;
	private String expected;
	
	@Before
	public void setup() {
		
		// 4 quantiles
		q4 = new Quantiles(arr, 4);
		
		// 3 quantiles
		q3 = new Quantiles(arr, 3);
		
	}

	@Test
	public void testGetQ4() {

		assertEquals("Expected 4 quantiles.", 4, q4.getQ());
		
	}
	
	@Test
	public void testSize4() {
		
		assertEquals("Expected array size is 10.", 10, q4.size());
		
	}
	
	@Test
	public void testQuantile4_1() {
		
		assertEquals("1st quartile should be 6", 6, q4.getQuantile(1));
		
	}
	
	@Test
	public void testQuantile4_2() {
	
		assertEquals("2nd quartile should be 11", 11, q4.getQuantile(2));
	
	}
	
	@Test
	public void testQuantile4_3() {
		
		assertEquals("3rd quartile should be 18", 18, q4.getQuantile(3));
		
	}
	
	@Test
	public void testBottomTotal4() {
		
		assertEquals("Expected bottom total is 13", 13, q4.getBottomTotal());
		
	}
	
	@Test
	public void testTopTotal4() {
		
		assertEquals("Expected top total is 58.", 58, q4.getTopTotal());
		
	}
	
	@Test
	public void testRatio4() {
		
		assertTrue("Unexpected ineqRatio.", Math.abs(4.46153f - q4.ineqRatio()) < 0.00001f);
		
	}

		
	@Test
	public void testGet3() {
		
		assertEquals("Expected 3 quantiles.", 3, q3.getQ());
	
	}
	
	@Test
	public void testSize3() {
		
		assertEquals("Expected array size is 10.", 10, q3.size());
		
	}
	
	@Test
	public void testQuantile3_1() {
		
		assertEquals("1st 3-quantile should be 9", 9, q3.getQuantile(1));
		
	}
	
	@Test
	public void testQuantile3_2() {
		
		assertEquals("2nd 3-quantile should be 17", 17, q3.getQuantile(2));
	
	}
	
	@Test
	public void testBottomTotal3() {
		
		assertEquals("Expected bottom total is 22", 22, q3.getBottomTotal());
	}
	
	@Test
	public void testTopTotal3() {
		
		assertEquals("Expected top total is 76.", 76, q3.getTopTotal());
		
	}
	
	@Test
	public void testRatio3() {
		
		assertTrue("Unexpected ineqRatio.", Math.abs(3.45454545f - q3.ineqRatio()) < 0.00001f);
	}

	@Test
	public void testOutput4() {
		
		expected = "10, 4, [6, 11, 18], 58, 13\n";
		assertTrue("Error - Output strings do not match.", expected.equals(q4.toString()));
	
	}
	
	@Test
	public void testOutput3() {
		
		expected = "10, 3, [9, 17], 76, 22\n";
		assertTrue("Error - Ouput strings do not match.", expected.equals(q3.toString()));
		
	}

	@Test
	public void testQuery1() {

		assertEquals("Expected quantile is I", 1, q4.quantileQuery(2));
		
	}
	
	@Test
	public void testQuery2() {
		
		assertEquals("Expected quantile is I", 1, q4.quantileQuery(6));
		
	}
	
	@Test
	public void testQuery3() {
		
		assertEquals("Expected quantile is II", 2, q4.quantileQuery(7));
		
	}
	
	@Test
	public void testQuery4() {
		
		assertEquals("Expected quantile is II", 2, q4.quantileQuery(11));
		
	}
	
	@Test
	public void testQuery5() {
		
		assertEquals("Expected quantile is III", 3, q4.quantileQuery(12));
		
	}
	
	@Test
	public void testQuery6() {
		
		assertEquals("Expected quantile is III", 3, q4.quantileQuery(13));
		
	}
	
	@Test
	public void testQuery7() {
		
		assertEquals("Expected quantile is III", 3, q4.quantileQuery(16));
		
	}
	
	@Test
	public void testQuery8() {
		
		assertEquals("Expected quantile is III", 3, q4.quantileQuery(18));
		
	}
	
	@Test
	public void testQuery9() {
		
		assertEquals("Expected quantile is IV", 4, q4.quantileQuery(20));
		
	}
	
}