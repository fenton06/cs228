package edu.iastate.cs228.hw2;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Before;

/**
 * Test OrderStatistics.java.
 * 
 * @author Benjamin Fenton
 */
public class OrderTest {
	
	private int[] arr1 = { 15, 17, 9, 35, 23, 2, 11, 18, 5, 6 };
	private int[] arr2 = { 6 };
	private int[] arr3 = { 2, 14, 1 };
	private int[] arr4 = new int[1000];
	
	@Before
	public void setup() {
		// setup arr4
		// initialize the array
		for(int i = 0; i < arr4.length; i++) {
			arr4[i] = i + 1;
		}
		
		// shuffle array
		shuffle(arr4);
		
		// sort the array
		Arrays.sort(arr4);
		
	}

	@Test
	public void testSelectMedSmall() {

		assertEquals("Minimum value should 2.", 2, OrderStatistics.findMinimum(arr1));

		assertEquals("Middle value should be 11.", 11, OrderStatistics.findMedian(arr1));

		assertEquals("Maximum value should be 35.", 35, OrderStatistics.findMaximum(arr1));
	}

	@Test
	public void testSelectMin() {
		
		assertEquals("Minimum value should be 6.", 6, OrderStatistics.findMinimum(arr2));
		
	}

	@Test
	public void testSelectSmall() {

		assertEquals("Minimum value should 1.", 1, OrderStatistics.findMinimum(arr3));

		assertEquals("Middle value should be 2.", 2, OrderStatistics.findMedian(arr3));

		assertEquals("Maximum value should be 14.", 14, OrderStatistics.findMaximum(arr3));
	}

	@Test
	public void testSelectLarge() {

		// find the max, med, and min 
		assertEquals("Minimum value should be " + arr4[0], arr4[0], OrderStatistics.findMinimum(arr4));

		assertEquals("Maximum value should be " + arr4[arr4.length - 1], arr4[arr4.length - 1], OrderStatistics.findMaximum(arr4));

		int med = (int) Math.ceil(arr4.length / 2f);
		
		assertEquals("Median value should be " + arr4[med - 1], arr4[med - 1], OrderStatistics.findMedian(arr4));

		// test some arbitrary values
		assertEquals("300th smallest value should be " + arr4[299], arr4[299], OrderStatistics.findOrderStatistic(arr4, 300));

		assertEquals("678th smallest value should be " + arr4[677], arr4[677], OrderStatistics.findOrderStatistic(arr4, 678));
	}

	@Test
	public void testSelectVeryLarge() {
		int index;
		int res;
		int[] arr = new int[99999];
		Random r = new Random(6);

		// initialize the array with no doubles
		for(int i = 0; i < arr.length; i++)
			arr[i] = i + 1;
		shuffle(arr);

		// clone and sort the array
		int[] sorted = arr.clone();
		Arrays.sort(sorted);

		// find the max, med, and min 
		res = OrderStatistics.findMinimum(arr);
		assertEquals("Minimum value should be " + sorted[0], sorted[0], res);

		res = OrderStatistics.findMaximum(arr);
		assertEquals("Maximum value should be " + sorted[arr.length - 1], sorted[arr.length - 1], res);

		int med = (int) Math.ceil(arr.length / 2f);
		res = OrderStatistics.findMedian(arr);
		assertEquals("Median value should be " + sorted[med - 1], sorted[med - 1], res);

		// test some arbitrary values
		index = r.nextInt(arr.length - 1) + 1;
		res = OrderStatistics.findOrderStatistic(arr, index);
		assertEquals("300th smallest value should be " + sorted[index - 1], sorted[index - 1], res);

		index = r.nextInt(arr.length - 1) + 1;
		res = OrderStatistics.findOrderStatistic(arr, index);
		assertEquals("300th smallest value should be " + sorted[index - 1], sorted[index - 1], res);

		index = r.nextInt(arr.length - 1) + 1;
		res = OrderStatistics.findOrderStatistic(arr, index);
		assertEquals("300th smallest value should be " + sorted[index - 1], sorted[index - 1], res);
	}

	private static void shuffle(int[] arr) {
		Random r = new Random(1);
		for(int i = 0; i < arr.length; i++) {
			int index = r.nextInt(arr.length);

			int tmp = arr[index];
			arr[index] = arr[i];
			arr[i] = tmp;
		}
	}
}