package edu.iastate.cs228.hw2;

import java.util.Random;

/**
 * A class used to generate order statistics of datasets
 * 
 * @author Benjamin Fenton
 */
public class OrderStatistics {

    /**
     * Returns the minimum element (first order statistic) in array arr. This
     * method must run in worst-case O(n) time, where n = arr.length, using a
     * linear scan of the input array.
     * 
     * @param arr
     * - The data to search
     * @return - the minimum element of arr
     */
    public static int findMinimum(int[] arr) {
    	
    	int small = arr[0];
    	
    	for(int i = 1; i < arr.length; i++) {
    		if(arr[i] < small) {
    			small = arr[i];
    		}
    	}
        return small;
    }

    /**
     * Returns the maximum element (first order statistic) in array arr. This
     * method must run in worst-case O(n) time, where n = arr.length, using a
     * linear scan of the input array.
     * 
     * @param arr
     * - The data to search
     * @return - the maximum element of arr
     */
    public static int findMaximum(int[] arr) {
    	
    	int big = arr[0];
    	
    	for(int i = 1; i < arr.length; i++) {
    		if(arr[i] > big) {
    			big = arr[i];
    		}
    	}
        return big;
    }

    /**
     * An implementation of the SELECT algorithm of Figure 1 of the project
     * specification. Returns the ith order statistic in the subarray
     * arr[first], ..., arr[last]. The method must run in O(n) expected time,
     * where n = (last - first + 1).
     * 
     * @param arr
     * - The data to search in
     * @param first
     * - The leftmost boundary of the subarray (inclusive)
     * @param last
     * - The rightmost boundary of the subarray (inclusive)
     * @param i
     * - The requested order statistic to find
     * @return - The ith order statistic in the subarray
     * 
     * @throws IllegalArgumentException
     * - If i < 1 or i > n
     */
    public static int select(int[] arr, int first, int last, int i) throws IllegalArgumentException {
    	
    	if(first == last) {
    		return arr[first];
    	}
    	
    	int p = partition(arr, first, last);
    	int k = p - first + 1;
    	
    	if(i == k) {
    		return arr[p];
    	} else if(i < k) {
    		return select(arr, first, p - 1, i);
    	} else {
    		return select(arr, p + 1, last, i - k);
    	}
    	
    }

    /**
     * Returns the ith order statistic of array arr in O(n) expected time, where
     * n = arr.length.
     * 
     * @param arr
     * - The data to search through
     * @param i
     * - The requested order statistic to find in arr
     * @return - The ith order statistic in arr
     * 
     * @throws IllegalArgumentException
     * - If i < 1 or i > n
     */
    public static int findOrderStatistic(int[] arr, int i) throws IllegalArgumentException {
        return select(arr, 0, arr.length - 1, i);
    }

    /**
     * Returns the median (n/2th order statistic rounding up) in array arr in
     * O(n) expected time, where n = arr.length.
     * 
     * @param arr
     * - The array to find the median of
     * @return
     * - The median value of arr
     */
    public static int findMedian(int[] arr) {
        
    	if(arr.length%2 == 0) {
    		return select(arr, 0, arr.length - 1, (arr.length)/2);
    	} else {
    		return select(arr, 0, arr.length - 1, (arr.length)/2 + 1);
    	}
    	
    	
    }
    
	private static int partition(int[] arr, int first, int last) {
    	
		// random pivot between first and last
		Random r = new Random();
		int pivIndex = r.nextInt((last - first) + 1) + first;
		int pivot = arr[pivIndex];
		
		//swap pivot to end
		int temp = arr[pivIndex];
		arr[pivIndex] = arr[last];
		arr[last] = temp;
		
    	int i = first - 1;
    	
    	for(int j = first; j < last; j++) { 
    		if(arr[j] <= pivot) {
    			i++;
    			// swap arr[i] and arr[j]
    			temp = arr[i];
    			arr[i] = arr[j];
    			arr[j] = temp;
    		}
    	}
    	
    	//swap arr[i+1] and arr[last];
    	temp = arr[i+1];
    	arr[i+1] = arr[last];
    	arr[last] = temp;
    	
    	return i+1;
    }
    
}
